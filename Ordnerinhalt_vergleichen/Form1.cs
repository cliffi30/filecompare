﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Ordnerinhalt_vergleichen
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        List<string> searchFilePaths = new List<string>();
        List<string> searchFileNames = new List<string>();

        List<string> targetFilesPaths = new List<string>();
        List<string> targetFilesNames = new List<string>();

        List<string> missingFilesPaths = new List<string>();
        List<string> foundFilesPathsTarget = new List<string>();
        List<string> foundFilesPathSource = new List<string>();

        private void button1_Click(object sender, EventArgs e)
        {
            using (var folder = new FolderBrowserDialog())
            {
                DialogResult mydialog = folder.ShowDialog();

                if (mydialog == DialogResult.OK && !string.IsNullOrWhiteSpace(folder.SelectedPath))
                {
                    searchFilePaths = GetAllFiles(folder.SelectedPath);
                    textBox1.Text = folder.SelectedPath;
                }
            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            using (var folder = new FolderBrowserDialog())
            {
                DialogResult mydialog = folder.ShowDialog();

                if (mydialog == DialogResult.OK && !string.IsNullOrWhiteSpace(folder.SelectedPath))
                {
                    targetFilesPaths = GetAllFiles(folder.SelectedPath);
                    textBox2.Text = folder.SelectedPath;
                }

                button3.Enabled = true;
            }
        }

        public static List<String> GetAllFiles(String directory)
        {
            return Directory.GetFiles(directory, "*.*", SearchOption.AllDirectories).ToList();
        }

        private void button3_Click(object sender, EventArgs e)
        {
            foreach (string file in searchFilePaths)
            {
                string[] array = file.Split('\\');
                searchFileNames.Add(array[array.Length - 1]);
            }

            foreach (string file in targetFilesPaths)
            {
                string[] array = file.Split('\\');
                targetFilesNames.Add(array[array.Length - 1]);
            }


            int counter = 0;

            foreach (string file in searchFileNames)
            {
                int temp = targetFilesNames.FindIndex(x => x==file);

                if (temp ==-1)
                {
                    missingFilesPaths.Add(searchFilePaths[counter]);
                }

                else
                {
                    foundFilesPathsTarget.Add(targetFilesPaths[temp]);
                    foundFilesPathSource.Add(searchFilePaths[counter]);
                }

                counter++;
            }

            Form Result = new Result(missingFilesPaths, foundFilesPathsTarget, foundFilesPathSource);
            Result.ShowDialog();
        }
    }
}

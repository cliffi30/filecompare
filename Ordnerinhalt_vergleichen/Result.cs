﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.IO;

namespace Ordnerinhalt_vergleichen
{
    public partial class Result : Form
    {

        protected List<string> missingFilePaths = new List<string>();
        protected List<string> foundFilePathsTarget = new List<string>();
        protected List<string> foundFilePathsSource = new List<string>();


        public Result(List<string> t_missingFilePaths, List<string> t_foundfilePathsTarget, List<string> t_foundFilesPathsSource)
        {

            missingFilePaths = t_missingFilePaths;
            foundFilePathsTarget = t_foundfilePathsTarget;
            foundFilePathsSource = t_foundFilesPathsSource;


            InitializeComponent();

            foreach (string path in missingFilePaths)
            {
                textBox1.Text += path;
                textBox1.Text += "\r\n";
            }

            label2.Text = missingFilePaths.Count.ToString();

            for (int a = 0; a < foundFilePathsSource.Count; a++)
            {
                textBox2.Text += foundFilePathsSource[a] + " >> " + foundFilePathsTarget[a];
                textBox2.Text += "\r\n";
            }

            label4.Text = foundFilePathsTarget.Count.ToString();


        }


        private void button1_Click(object sender, EventArgs e)
        {

            using (var savefile = new SaveFileDialog())
            {

                savefile.FileName = "unknown.txt";

                savefile.Filter = "Text files (*.txt)|*.txt";

                if (savefile.ShowDialog() == DialogResult.OK)
                {
                    var w = new StreamWriter(savefile.FileName);

                    foreach (string line in missingFilePaths)
                    {
                        var lineout = string.Format("{0}", line);
                        w.WriteLine(lineout);
                        w.Flush();
                    }
                }

            }
        }


        private void button2_Click(object sender, EventArgs e)
        {
            using (var savefile = new SaveFileDialog())
            {

                savefile.FileName = "unknown.txt";

                savefile.Filter = "Text files (*.txt)|*.txt";

                if (savefile.ShowDialog() == DialogResult.OK)
                {
                    var w = new StreamWriter(savefile.FileName);

                    for (int a = 0; a < foundFilePathsTarget.Count; a++)
                    {
                        var lineout = string.Format("{0} >> {1}", foundFilePathsSource[a], foundFilePathsTarget[a]);
                        w.WriteLine(lineout);
                        w.Flush();
                    }
                }
            }
        }
    }
}
